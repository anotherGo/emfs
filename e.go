package emfs

import "errors"

var ErrIsOpen = errors.New("The file is open")
