package emfs

type Whence int

const (
	FileBegin Whence = 0
	FileEnd   Whence = 2
	LastSeek  Whence = 1
)
