package emfs

import "os"

const DefaultMode = uint32(0666)
const DefaultFlag = FlagForOpen(os.O_RDWR | os.O_CREATE)
