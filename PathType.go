package emfs

import (
	"os"
	"path/filepath"
)

type PathType uint8

const (
	IsUnknown PathType = 0
	IsDirectory
	IsFile
)

func TypeOf(path ...string) PathType {
	stat, e := os.Stat(filepath.Join(path...))
	if e != nil {
		return IsUnknown
	}
	if stat.IsDir() {
		return IsDirectory
	}
	return IsFile
}
