package emfs

import (
	"os"
	"path/filepath"
)

type Fs struct {
	root string
}

//OpenFile opens a file
func (fs *Fs) OpenFile(path string, flags ...FlagForOpen) (File, error) {
	// combine flags
	var flag FlagForOpen
	for _, f := range flags {
		flag |= f
	}
	// join with root
	safePath := filepath.Join(fs.root, path)
	f, e := os.OpenFile(safePath, int(flag), os.FileMode(DefaultMode))
	return File{osFile: f}, e
}
