package emfs

import "os"

type FlagForOpen int

const (
	Rd   FlagForOpen = FlagForOpen(os.O_RDONLY)
	Wr   FlagForOpen = FlagForOpen(os.O_WRONLY)
	RW   FlagForOpen = FlagForOpen(os.O_RDWR)
	Ap   FlagForOpen = FlagForOpen(os.O_APPEND)
	Cr   FlagForOpen = FlagForOpen(os.O_CREATE)
	Crx  FlagForOpen = FlagForOpen(os.O_EXCL | os.O_CREATE)
	Tr   FlagForOpen = FlagForOpen(os.O_TRUNC)
	Sync FlagForOpen = FlagForOpen(os.O_SYNC)
)
